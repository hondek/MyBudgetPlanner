package com.quadrapps.budgetplanner.converters

import android.util.Log
import android.widget.TextView
import androidx.databinding.BindingAdapter
import java.text.SimpleDateFormat
import java.util.*

@BindingAdapter("dateFormatter")
fun TextView.dateFormatter(date: Long?) {
    Log.d("EEE", date.toString())
    if (date != null) {
        // var dateFormat = Date(date!!)
        val format = SimpleDateFormat("dd.MM.yyyy")
        this.text = format.format((date * 1000))
    }
}