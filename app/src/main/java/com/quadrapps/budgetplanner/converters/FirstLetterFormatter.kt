package com.quadrapps.budgetplanner.converters

import android.widget.TextView
import androidx.databinding.BindingAdapter

@BindingAdapter("firstLetterFormatter")
fun TextView.firstLetterFormatter(name: String?) {
    this.text = name?.get(0).toString()
}