package com.quadrapps.budgetplanner.converters

import android.widget.TextView
import androidx.databinding.BindingAdapter

@BindingAdapter("floatFormatter")
fun TextView.floatFormatter(number: Float?) {
    this.text = number?.toString()
}

@BindingAdapter("longFormatter")
fun TextView.longFormatter(number: Long?) {
    this.text = number?.toString()
}
