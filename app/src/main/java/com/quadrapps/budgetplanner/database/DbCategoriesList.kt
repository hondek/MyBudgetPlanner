package com.quadrapps.budgetplanner.database

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.quadrapps.budgetplanner.domain.CategoriesListItem
import com.quadrapps.budgetplanner.domain.CategoryDetails

@Entity
data class DbCategoriesList constructor(
    @PrimaryKey(autoGenerate = true)
    val cid: Long,
    val name: String,
    val type: Int,
    val icon: String,
    val color: Int,
)

fun List<DbCategoriesList>.asDomainModel(): List<CategoriesListItem> {
    return map {
        CategoriesListItem(
            id = it.cid,
            name = it.name,
            type = it.type,
            icon = it.icon,
            color = it.color
        )
    }
}

fun DbCategoriesList.asDomainModel(): CategoryDetails {
    return CategoryDetails(
        id = cid,
        name = name,
        type = type,
        icon = icon,
        color = color,
    )
}