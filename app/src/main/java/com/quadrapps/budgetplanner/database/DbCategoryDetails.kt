package com.quadrapps.budgetplanner.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.quadrapps.budgetplanner.domain.CategoryDetails

@Entity
data class DbCategoryDetails constructor(
    @PrimaryKey
    val cid: Long,
    val name: String,
    val type: Int, // 0 - expenses, 1 - income
    val icon: String,
    val color: Int,
)

fun DbCategoryDetails.asDomainModel(): CategoryDetails {
    return CategoryDetails(
        id = cid,
        name = name,
        type = type,
        icon = icon,
        color = color,
    )
}