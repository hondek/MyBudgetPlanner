package com.quadrapps.budgetplanner.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.quadrapps.budgetplanner.database.converters.DateConverter
import com.quadrapps.budgetplanner.domain.TransactionDetails
import com.quadrapps.budgetplanner.domain.TransactionsListItem
import java.util.*

@Entity
data class DbTransactionDetails constructor(
    @PrimaryKey
    val id: Long,
    val title: String,
    val category_id: Long,
    val amount: Float,
    val date: Long,
    val category_name: String? = "",
    val category_color: Int? = 0,
)

fun DbTransactionDetailsWithCategory.asDomainModel(): TransactionDetails {
    return TransactionDetails(
        id = transaction.id,
        title = transaction.title,
        category_id = transaction.category_id,
        amount = transaction.amount,
        date = transaction.date,
        category_name = category.name,
        category_color = category.color,
    )
}

fun DbTransactionDetails.asDomainModel(): TransactionDetails {
    return TransactionDetails(
        id = id,
        title = title,
        category_id = category_id,
        amount = amount,
        date = date,
    )
}