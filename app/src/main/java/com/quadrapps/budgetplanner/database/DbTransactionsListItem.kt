package com.quadrapps.budgetplanner.database

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.NO_ACTION
import androidx.room.PrimaryKey
import com.quadrapps.budgetplanner.domain.TransactionsListItem

/*(
foreignKeys = [
ForeignKey(
entity = DbCategoriesList::class,
parentColumns = ["id"],
childColumns = ["category_id"],
onDelete = NO_ACTION
)
]
)*/

@Entity
data class DbTransactionsListItem constructor(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    val title: String,
    val category_id: Long,
    val amount: Float,
    val date: Long,
)


fun List<TransactionsWithCategories>.asDomainModel(): List<TransactionsListItem> {
    return map {
        TransactionsListItem(
            id = it.transaction.id,
            title = it.transaction.title,
            category_id = it.transaction.category_id,
            amount = it.transaction.amount,
            date = it.transaction.date,
            category_name = it.category.name,
            category_color = it.category.color,
        )
    }
}
/*
fun List<DbTransactionsListItem>.asDomainModel(): List<TransactionsListItem> {
    return map {
        TransactionsListItem(
            id = it.id,
            title = it.title,
            category_id = it.category_id,
            category_name = "",
            amount = it.amount,
            date = it.date,
        )
    }
}*/

