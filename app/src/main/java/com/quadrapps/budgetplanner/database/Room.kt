package com.quadrapps.budgetplanner.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.quadrapps.budgetplanner.database.converters.DateConverter
import com.quadrapps.budgetplanner.domain.CategoryDetails
import com.quadrapps.budgetplanner.domain.TransactionDetails

data class TransactionsWithCategories(
    @Embedded
    val transaction: DbTransactionsListItem,
    @Embedded
    val category: DbCategoriesList,
)

data class TransactionsWithCategoriesWithAmountSum(
    @Embedded
    val transaction: DbTransactionsListItem,
    @Embedded
    val category: DbCategoriesList,
)

data class DbTransactionDetailsWithCategory(
    @Embedded
    val transaction: DbTransactionsListItem,
    @Embedded
    val category: DbCategoriesList,
)

@Dao
interface TransactionsDao {

    @Query("select * from DbTransactionsListItem LEFT JOIN DbCategoriesList ON DbTransactionsListItem.category_id = DbCategoriesList.cid WHERE DbCategoriesList.type = :type")
    fun getDbTransactionsWithCategoriesList(type: Int): LiveData<List<TransactionsWithCategories>>

    @Query("select * from DbTransactionsListItem")
    fun getDbTransactionsList(): LiveData<List<DbTransactionsListItem>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllTransactions(transactions: List<DbTransactionsListItem>)

    @Query("select * from DbTransactionsListItem LEFT JOIN DbCategoriesList ON DbTransactionsListItem.category_id = DbCategoriesList.cid WHERE DbTransactionsListItem.id = :id")
    fun getTransactionById(id: Long): DbTransactionDetailsWithCategory

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSingleTransaction(transaction: DbTransactionDetails)

    @Query("insert into DbTransactionsListItem (id, title, category_id, amount, date) VALUES (NULL, 'Przychod1', 0, 100, 0), (NULL, 'Przychod2', 0, 50, 0), (NULL, 'Wydatek1', 1, 100, 0), (NULL, 'Wydatek2', 1, 88, 0)")
    fun insertInitData()

    @Query("SELECT SUM(DbTransactionsListItem.amount) as amount, * from DbTransactionsListItem LEFT JOIN DbCategoriesList ON DbTransactionsListItem.category_id = DbCategoriesList.cid WHERE DbCategoriesList.type = :type GROUP BY DbCategoriesList.cid")
    fun getTransactionsForChart(type: Int): List<TransactionsWithCategoriesWithAmountSum>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateTransaction(value: DbTransactionsListItem)
}



@Dao
interface CategoriesDao {

    @Query("select * from DbCategoriesList WHERE type = :type")
    fun getDbCategoriesList(type: Int): LiveData<List<DbCategoriesList>>

    @Query("select * from DbCategoriesList where cid = :categoryId")
    fun getCategoryById(categoryId: Long): DbCategoriesList

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateCategory(currentCategory: DbCategoriesList)

    @Query("insert into DbCategoriesList (cid, name, type, icon, color) VALUES (NULL, 'Family', 0, '', -16777216), (NULL, 'Education', 0, '', -16777216), (NULL, 'Home', 0, '', -16777216), (NULL, 'Paycheck', 1, '', -16777216), (NULL, 'Gift', 1, '', -16777216), (NULL, 'Interest', 1, '', -16777216)  ")
    fun insertInitialCategories()
}

@Database(entities = [DbTransactionsListItem::class, DbTransactionDetails::class, DbCategoriesList::class, DbCategoryDetails::class], version = 1, exportSchema = false)
@TypeConverters(DateConverter::class)
abstract class TransactionsDatabase : RoomDatabase() {
    abstract val transactionsDao: TransactionsDao
    abstract val categoriesDao: CategoriesDao
}