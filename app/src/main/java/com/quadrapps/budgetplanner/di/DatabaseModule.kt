package com.quadrapps.budgetplanner.di

import android.content.Context
import androidx.room.Room
import com.quadrapps.budgetplanner.database.TransactionsDao
import com.quadrapps.budgetplanner.database.TransactionsDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {
    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): TransactionsDatabase {
        return Room.databaseBuilder(
            appContext,
            TransactionsDatabase::class.java,
            "Transactions"
        ).fallbackToDestructiveMigration().build()
    }

    @Provides
    fun provideChannelDao(transactionsDatabase: TransactionsDatabase): TransactionsDao {
        return transactionsDatabase.transactionsDao
    }

}