package com.quadrapps.budgetplanner.di

import com.quadrapps.budgetplanner.network.TransactionDetailsService
import com.quadrapps.budgetplanner.network.TransactionsListService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModele {

    @Singleton
    @Provides
    fun provideOkHttpClient() = OkHttpClient.Builder().build()

    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create())
        .baseUrl("http://137.74.92.81/budgetApi/")
        .client(okHttpClient)
        .build()

    @Singleton
    @Provides
    fun provideApiService(retrofit: Retrofit): TransactionsListService =
        retrofit.create(TransactionsListService::class.java)

    @Singleton
    @Provides
    fun provideTransactionDetailsService(retrofit: Retrofit): TransactionDetailsService =
        retrofit.create(TransactionDetailsService::class.java)

}