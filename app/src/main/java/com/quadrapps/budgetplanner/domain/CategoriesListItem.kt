package com.quadrapps.budgetplanner.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CategoriesListItem(
    val id: Long? = 0,
    val name: String? = "",
    val type: Int? = 0,
    val icon: String? = "",
    val color: Int? = 0x00000000
) : Parcelable