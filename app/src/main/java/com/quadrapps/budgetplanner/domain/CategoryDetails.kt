package com.quadrapps.budgetplanner.domain

import android.os.Parcelable
import com.quadrapps.budgetplanner.database.DbCategoriesList
import com.quadrapps.budgetplanner.database.DbCategoryDetails
import kotlinx.parcelize.Parcelize

@Parcelize
data class CategoryDetails(
    val id: Long? = 0,
    val name: String? = "",
    var type: Int? = 0,
    val icon: String? = "",
    var color: Int? = -16777216
) : Parcelable

fun CategoryDetails.toDbObject(): DbCategoriesList {
    return DbCategoriesList(
        cid = this.id!!,
        name = this.name!!,
        type = this.type!!,
        icon = this.icon!!,
        color = this.color!!
    )
}