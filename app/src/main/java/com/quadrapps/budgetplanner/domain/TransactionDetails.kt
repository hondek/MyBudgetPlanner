package com.quadrapps.budgetplanner.domain

import android.os.Parcelable
import com.quadrapps.budgetplanner.database.DbTransactionDetails
import com.quadrapps.budgetplanner.database.DbTransactionsListItem
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class TransactionDetails(
    val id: Long? = 0,
    val title: String? = "",
    val category_id: Long? = 0,
    val amount: Float? = 0f,
    var date: Long? = 0,
    val category_name: String? = "",
    val category_color: Int? = 0,
) : Parcelable


fun TransactionDetails.toDbObject(): DbTransactionsListItem {
    return DbTransactionsListItem(
        id = this.id!!,
        title = this.title!!,
        category_id = this.category_id!!,
        amount = this.amount!!,
        date = this.date!!,
    )
}