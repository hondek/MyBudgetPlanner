package com.quadrapps.budgetplanner.domain

import java.util.Date

data class TransactionsListItem (
    val id: Long,
    val title: String,
    val category_id: Long,
    val category_name: String,
    val category_color: Int,
    val amount: Float,
    val date: Long,
)