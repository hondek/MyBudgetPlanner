package com.quadrapps.budgetplanner.domain

enum class TypesEnum(val type: Int) {
    EXPENSES(0),
    INCOME(1)
}