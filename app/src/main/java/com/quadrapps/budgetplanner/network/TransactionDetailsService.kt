package com.quadrapps.budgetplanner.network

import com.quadrapps.budgetplanner.network.models.NetTransactionDetails
import retrofit2.http.GET
import retrofit2.http.Path

interface TransactionDetailsService {
    @GET("transactions/getById/{transactionId}")
    suspend fun getTransactionById(@Path("transactionId") transactionId: Long): NetTransactionDetails
}