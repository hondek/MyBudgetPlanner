package com.quadrapps.budgetplanner.network

import com.quadrapps.budgetplanner.network.models.NetTransactionsListItem
import retrofit2.http.GET

interface TransactionsListService {
    @GET("transactions/getAll")
    suspend fun getTransactions(): List<NetTransactionsListItem>
}