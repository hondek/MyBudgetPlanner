package com.quadrapps.budgetplanner.network.models

import com.quadrapps.budgetplanner.database.DbTransactionDetails
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class NetTransactionDetails (
    @Json(name = "id")
    val id: Long,
    @Json(name = "title")
    val title: String,
    @Json(name = "category_id")
    val category_id: Long,
    @Json(name = "category_name")
    val category_name: String,
    @Json(name = "amount")
    val amount: Float,
    @Json(name = "date")
    val date: Long,
)

fun NetTransactionDetails.asDatabaseModel(): DbTransactionDetails {
    return DbTransactionDetails(
        id = id,
        title = title,
        category_id = category_id,
        amount = amount,
        date = date,
    )
}