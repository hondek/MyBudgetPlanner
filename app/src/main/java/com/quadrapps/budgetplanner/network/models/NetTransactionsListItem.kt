package com.quadrapps.budgetplanner.network.models

import com.quadrapps.budgetplanner.database.DbTransactionsListItem
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class NetTransactionsListItem (
    @Json(name = "id")
    val id: Long,
    @Json(name = "title")
    val title: String,
    @Json(name = "category_id")
    val category_id: Long,
    @Json(name = "category_name")
    val category_name: String,
    @Json(name = "amount")
    val amount: Float,
    @Json(name = "date")
    val date: Long,
)

fun List<NetTransactionsListItem>.asDatabaseModel(): List<DbTransactionsListItem> {
    return map {
        DbTransactionsListItem(
            id = it.id,
            title = it.title,
            category_id = it.category_id,
            amount = it.amount,
            date = it.date,
        )
    }
}