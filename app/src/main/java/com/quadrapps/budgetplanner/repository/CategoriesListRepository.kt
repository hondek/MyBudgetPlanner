package com.quadrapps.budgetplanner.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.quadrapps.budgetplanner.database.DbCategoryDetails
import com.quadrapps.budgetplanner.database.TransactionsDatabase
import com.quadrapps.budgetplanner.database.asDomainModel
import com.quadrapps.budgetplanner.domain.CategoriesListItem
import com.quadrapps.budgetplanner.domain.CategoryDetails
import com.quadrapps.budgetplanner.domain.TypesEnum
import com.quadrapps.budgetplanner.domain.toDbObject
import javax.inject.Inject

class CategoriesListRepository @Inject constructor(
    private val database: TransactionsDatabase
) {

    fun getCategories(type: Int): LiveData<List<CategoriesListItem>> {
        return Transformations.map(database.categoriesDao.getDbCategoriesList(type)) {
            it.asDomainModel()
        }
    }

    fun getCategoryByCid(cid: Long): CategoryDetails
    {
        return database.categoriesDao.getCategoryById(cid).asDomainModel()
    }

    /*val categories: LiveData<List<CategoriesListItem>> =
        Transformations.map(database.categoriesDao.getDbCategoriesList()) {
            it.asDomainModel()
        }*/

    fun putInitialData() {
        database.categoriesDao.insertInitialCategories()
    }

    fun updateCategory(currentCategory: CategoryDetails) {
        var category = currentCategory.toDbObject()
        database.categoriesDao.updateCategory(category)
    }

}