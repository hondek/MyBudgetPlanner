package com.quadrapps.budgetplanner.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.liveData
import com.quadrapps.budgetplanner.database.DbCategoriesList
import com.quadrapps.budgetplanner.database.TransactionsDatabase
import com.quadrapps.budgetplanner.database.asDomainModel
import com.quadrapps.budgetplanner.domain.CategoriesListItem
import com.quadrapps.budgetplanner.domain.CategoryDetails
import javax.inject.Inject

class CategoryDetailsRepository @Inject constructor(
    private val database: TransactionsDatabase
) {

    val data: LiveData<List<CategoriesListItem>> =
        Transformations.map(database.categoriesDao.getDbCategoriesList(0)) {
            it?.asDomainModel()
        }

    fun getCategoryDetails(categoryId: Long): LiveData<CategoryDetails> {
        return liveData { database.categoriesDao.getCategoryById(categoryId).asDomainModel() }
    }

}