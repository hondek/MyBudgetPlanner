package com.quadrapps.budgetplanner.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.quadrapps.budgetplanner.database.TransactionsDatabase
import com.quadrapps.budgetplanner.database.asDomainModel
import com.quadrapps.budgetplanner.domain.TransactionDetails
import com.quadrapps.budgetplanner.network.TransactionDetailsService
import com.quadrapps.budgetplanner.network.models.asDatabaseModel
import javax.inject.Inject

class TransactionDetailsRepository @Inject constructor(
    private val database: TransactionsDatabase,
    private val transactionDetailsService: TransactionDetailsService
) {

    fun getTransactionDetails(transactionId: Long): TransactionDetails {
        return database.transactionsDao.getTransactionById(transactionId).asDomainModel()
        /*return Transformations.map() {
            it?.asDomainModel()
        }*/
    }

    suspend fun refreshTransactionDetails(transactionId: Long) {
        try {
            val transactionDetails = transactionDetailsService.getTransactionById(transactionId)
            Log.d("EEE", transactionDetails.title)
            database.transactionsDao.insertSingleTransaction(transactionDetails.asDatabaseModel())
        } catch (e: Exception) {
            Log.d("EEE", e.toString())
        }
    }

}