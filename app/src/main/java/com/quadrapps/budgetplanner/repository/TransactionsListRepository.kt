package com.quadrapps.budgetplanner.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.liveData
import com.quadrapps.budgetplanner.database.DbTransactionsListItem
import com.quadrapps.budgetplanner.database.TransactionsDatabase
import com.quadrapps.budgetplanner.database.TransactionsWithCategoriesWithAmountSum
import com.quadrapps.budgetplanner.database.asDomainModel
import com.quadrapps.budgetplanner.domain.TransactionDetails
import com.quadrapps.budgetplanner.domain.TransactionsListItem
import com.quadrapps.budgetplanner.domain.toDbObject
import com.quadrapps.budgetplanner.network.TransactionsListService
import com.quadrapps.budgetplanner.network.models.NetTransactionsListItem
import com.quadrapps.budgetplanner.network.models.asDatabaseModel
import javax.inject.Inject

class TransactionsListRepository @Inject constructor(
    private val database: TransactionsDatabase,
    private val transactionsListService: TransactionsListService
) {
    /*val transactions: LiveData<List<TransactionsListItem>> =
        Transformations.map(database.transactionsDao.getDbTransactionsList()) {
            it.asDomainModel()
        }*/

    fun getTransactionsList(type: Int): LiveData<List<TransactionsListItem>> {
        val lst = database.transactionsDao.getDbTransactionsWithCategoriesList(type)
        // Log.d("EEE", lst.value?.size.toString())

        return Transformations.map(lst) {
            it.asDomainModel()
        }
    }

    fun getTransactionById(transactionId: Long): TransactionDetails {
        return database.transactionsDao.getTransactionById(transactionId).asDomainModel()
        /*return Transformations.map() {
            it?.asDomainModel()
        }*/
    }

    /*fun getTransactionById(id: Long): LiveData<TransactionsListItem> {
        return Transformations.map(database.transactionsDao.getTransactionById(id)) {
            it?.asDomainModel()
        }
    }*/

    suspend fun refreshTransactionsList() {
        try {
            // val transactions = transactionsListService.getTransactions()

            val transactions = listOf<DbTransactionsListItem>(
                DbTransactionsListItem(1, "Wydatek 1", 1, 400f, 0),
                DbTransactionsListItem(2, "Wydatek 2", 2, 55f, 0),
                DbTransactionsListItem(3, "Przychod 1", 4, 1000f, 0),
                DbTransactionsListItem(4, "Przychod 2", 5, 143f, 0),
            )


            database.transactionsDao.insertAllTransactions(transactions)
        } catch (e: Exception)
        {
            Log.d("EEE", e.toString())
        }
    }

    fun putInitData() {
        database.transactionsDao.insertInitData()
    }

    fun getTransactionsForChart(type: Int): List<TransactionsWithCategoriesWithAmountSum> {
        return database.transactionsDao.getTransactionsForChart(type)
    }

    fun updateTransaction(value: TransactionDetails) {
        val obj = value.toDbObject()
        database.transactionsDao.updateTransaction(obj)
    }
}