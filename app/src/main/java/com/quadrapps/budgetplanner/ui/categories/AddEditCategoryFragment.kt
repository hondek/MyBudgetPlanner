package com.quadrapps.budgetplanner.ui.categories

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import com.quadrapps.budgetplanner.R
import com.quadrapps.budgetplanner.databinding.FragmentAddEditCategoryBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class AddEditCategoryFragment : Fragment() {
    private val viewModel: CategoriesViewModel by viewModels()
    private var _binding: FragmentAddEditCategoryBinding? = null
    private val binding get() = _binding!!
    private val args: AddEditCategoryFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setCurrentCategory(args.cid)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_add_edit_category, container, false
        )
        binding.lifecycleOwner = viewLifecycleOwner

        // viewModel.setCurrentCategory(1L);
        binding.circleOuter.setOnClickListener {
            ColorPickerDialogBuilder
                .with(context)
                .setTitle("Choose color")
                .initialColor(viewModel.currentCategory.color!!)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setOnColorSelectedListener { selectedColor ->
                    viewModel.currentCategory.color = selectedColor
                    viewModel.onColorSelected(selectedColor)
                }
                .setPositiveButton(
                    "ok"
                ) { dialog, selectedColor, allColors -> Log.d("EEE", Integer.toHexString(selectedColor)) }
                .setNegativeButton(
                    "cancel"
                ) { dialog, which -> }
                .build()
                .show()
        }

        binding.saveButton.setOnClickListener {
            viewModel.saveCategory()
            findNavController().popBackStack()
        }

        viewModel.colorSelected.observe(viewLifecycleOwner) { color ->
            binding.circleView.circleColor = color
        }

        binding.viewModel = viewModel

        return binding.root
    }

}