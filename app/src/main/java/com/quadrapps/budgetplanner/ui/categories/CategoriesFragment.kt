package com.quadrapps.budgetplanner.ui.categories

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.quadrapps.budgetplanner.R
import com.quadrapps.budgetplanner.databinding.FragmentCategoriesBinding
import com.quadrapps.budgetplanner.domain.TypesEnum
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CategoriesFragment constructor(private val type: Int) : Fragment() {

    private val viewModel: CategoriesViewModel by viewModels()

    @Inject
    lateinit var adapter: CategoriesListAdapter

    private var _binding: FragmentCategoriesBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_categories, container, false
        )
        binding.lifecycleOwner = viewLifecycleOwner
        binding.categoriesList.layoutManager = GridLayoutManager(activity, 2)
        binding.categoriesList.adapter = adapter
        adapter.clickListener.onItemClick = {
            // TODO: implement navigation to category edit
            Log.d("EEE", "Navigating to category edit ID: ${it.id} NAME: ${it.name}")
            val bundle = Bundle()
            bundle.putLong("cid", it.id!!)
            findNavController().navigate(R.id.category_add_edit, bundle)
        }

        viewModel.initialize(type);

        binding.fab.setOnClickListener {
            val bundle = Bundle()
            bundle.putLong("cid", 0)
            findNavController().navigate(R.id.category_add_edit, bundle)
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.data.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.categoriesList.adapter = null
        _binding = null
    }

}