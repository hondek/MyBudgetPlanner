package com.quadrapps.budgetplanner.ui.categories

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.quadrapps.budgetplanner.databinding.ItemCategoriesListBinding
import com.quadrapps.budgetplanner.domain.CategoriesListItem
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class CategoriesListAdapter @Inject constructor(val clickListener: ClickCategoryListener)
    : ListAdapter<CategoriesListItem, CategoriesListAdapter.ViewHolder>(CategoriesListDiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, clickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(private val binding: ItemCategoriesListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: CategoriesListItem, clickListener: ClickCategoryListener) {
            binding.data = item
            binding.executePendingBindings()
            binding.clickListener = clickListener
            binding.circleView.circleColor = item.color!!
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemCategoriesListBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class CategoriesListDiffCallback : DiffUtil.ItemCallback<CategoriesListItem>() {

    override fun areItemsTheSame(oldItem: CategoriesListItem, newItem: CategoriesListItem): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: CategoriesListItem, newItem: CategoriesListItem): Boolean {
        return oldItem == newItem
    }
}

class ClickCategoryListener @Inject constructor() {

    var onItemClick: ((CategoriesListItem) -> Unit)? = null

    fun onClick(data: CategoriesListItem) {
        onItemClick?.invoke(data)
    }
}