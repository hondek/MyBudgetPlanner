package com.quadrapps.budgetplanner.ui.categories

import android.graphics.Color
import androidx.lifecycle.*
import com.quadrapps.budgetplanner.domain.CategoriesListItem
import com.quadrapps.budgetplanner.domain.CategoryDetails
import com.quadrapps.budgetplanner.repository.CategoriesListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class CategoriesViewModel @Inject constructor(
    private val categoriesListRepository: CategoriesListRepository
) : ViewModel() {

    lateinit var data: LiveData<List<CategoriesListItem>> // = categoriesListRepository.getCategories()
    lateinit var currentCategory: CategoryDetails

    fun initialize(type: Int) {
        data = categoriesListRepository.getCategories(type)
    }

    private val _colorSelected = MutableLiveData<Int>()
    val colorSelected: LiveData<Int> = _colorSelected
    fun onColorSelected(colorInt: Int) {
        _colorSelected.value = colorInt
    }

    private val _optionSelected = MutableLiveData<Int>()
    val optionSelected: LiveData<Int> = _optionSelected
    fun onOptionSelected(option: Int) {
        _optionSelected.value = option
        currentCategory.type = option
    }

    fun setCurrentCategory(cid: Long) {
        if (cid == 0L) {
            currentCategory = CategoryDetails(0L, "New category", 0, "")
            return
        }
        viewModelScope.launch(Dispatchers.IO) {
            currentCategory = categoriesListRepository.getCategoryByCid(cid)
            withContext(Dispatchers.Main) {
                _optionSelected.value = currentCategory.type!!
                _colorSelected.value = currentCategory.color!!
            }
        }
    }

    fun saveCategory() {
        viewModelScope.launch(Dispatchers.IO) {
            categoriesListRepository.updateCategory(currentCategory)
        }
    }


    init {
        viewModelScope.launch(Dispatchers.IO) {
            // categoriesListRepository.putInitialData()
        }
    }

}