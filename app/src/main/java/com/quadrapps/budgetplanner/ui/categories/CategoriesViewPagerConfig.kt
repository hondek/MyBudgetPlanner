package com.quadrapps.budgetplanner.ui.categories

import com.quadrapps.budgetplanner.R
import com.quadrapps.budgetplanner.domain.TypesEnum

interface CategoriesViewPagerConfig {
    companion object {
        val tabList = listOf(
            R.string.expenses_tab, R.string.income_tab
        )
        val pagerFragments = listOf(
            CategoriesFragment(0), CategoriesFragment(1)
        )
    }
}