package com.quadrapps.budgetplanner.ui.home

import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.quadrapps.budgetplanner.R
import com.quadrapps.budgetplanner.databinding.FragmentAddEditTransactionBinding
import com.quadrapps.budgetplanner.databinding.FragmentHomeBinding
import com.quadrapps.budgetplanner.ui.transactionDetails.TransactionDetailsFragmentArgs
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class AddEditTransactionFragment : Fragment(), DatePickerDialog.OnDateSetListener {

    private val viewModel: TransactionsListViewModel by viewModels()
    private var _binding: FragmentAddEditTransactionBinding? = null
    private val binding get() = _binding!!
    private val args: AddEditTransactionFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getTransactionById(args.transaction)
    }

    private fun showDatePickerDialog() {

    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, dayOfMonth)

        val timestamp = calendar.timeInMillis
        Log.d("EEE2", timestamp.toString())

        viewModel.setTransactionDate(timestamp)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_add_edit_transaction, container, false
        )
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        binding.changeDateButton.setOnClickListener {
            val calendar = Calendar.getInstance()
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val day = calendar.get(Calendar.DAY_OF_MONTH)

            val datePickerDialog = DatePickerDialog(requireContext(), this, year, month, day)
            datePickerDialog.show()
        }

        binding.saveButton.setOnClickListener {
            viewModel.saveTransaction(binding.editTitle.text.toString(), binding.editAmount.text.toString())
        }

        return binding.root
    }

}