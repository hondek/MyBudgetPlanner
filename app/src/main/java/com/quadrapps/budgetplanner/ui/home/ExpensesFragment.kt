package com.quadrapps.budgetplanner.ui.home

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.quadrapps.budgetplanner.R
import com.quadrapps.budgetplanner.databinding.FragmentExpensesBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ExpensesFragment : Fragment() {
    private val viewModel: TransactionsListViewModel by viewModels()

    @Inject
    lateinit var adapter: TransactionsListAdapter

    private var _binding: FragmentExpensesBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        increaseAppLaunchCount()

        val appLaunchCount = getAppLaunchCount()
        if (appLaunchCount == 1) {
            viewModel.putInitialData()
        }
    }

    private val PREFS_NAME = "PREFS"
    private val PREF_KEY_APP_LAUNCH_COUNT = "AppLaunchCount"

    private fun increaseAppLaunchCount() {
        val prefs: SharedPreferences = requireContext()
            .getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        val count: Int = prefs.getInt(PREF_KEY_APP_LAUNCH_COUNT, 0) + 1
        prefs.edit().putInt(PREF_KEY_APP_LAUNCH_COUNT, count).apply()
    }

    private fun getAppLaunchCount(): Int {
        val prefs: SharedPreferences = requireContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        return prefs.getInt(PREF_KEY_APP_LAUNCH_COUNT, 0)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_expenses, container, false
        )
        binding.lifecycleOwner = viewLifecycleOwner
        binding.recyclerView.adapter = adapter

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.initialize(0)
        viewModel.data.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }
        adapter.clickListener.onItemClick = {
            // findNavController().navigate(ExpensesFragmentDirections.actionExpensesListToTransactionDetails(it.id))
            val bundle = Bundle()
            bundle.putLong("transaction", it.id)
            bundle.putLong("cid", it.category_id)
            findNavController().navigate(R.id.transaction_details, bundle)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.recyclerView.adapter = null
        _binding = null
    }
}