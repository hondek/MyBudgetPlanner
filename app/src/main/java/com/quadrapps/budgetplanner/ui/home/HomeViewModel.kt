package com.quadrapps.budgetplanner.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.quadrapps.budgetplanner.domain.TransactionsListItem
import com.quadrapps.budgetplanner.repository.TransactionsListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val transactionsListRepository: TransactionsListRepository
) : ViewModel() {

    private val _pieData = MutableLiveData<PieData>()
    val pieData: LiveData<PieData> get() = _pieData

    fun putInitialData() {
        viewModelScope.launch(Dispatchers.IO) {
            transactionsListRepository.refreshTransactionsList()
            transactionsListRepository.putInitData()
        }
    }

    fun addNewTransaction() {
        Log.d("EEE", "EEE")
    }

    fun GetPieData(type: Int) {

        viewModelScope.launch(Dispatchers.IO) {
            val data = transactionsListRepository.getTransactionsForChart(type)

            Log.d("EEE2", data.size.toString())

            val entries = mutableListOf<PieEntry>()
            val colors = mutableListOf<Int>()

            data.forEach {
                entries.add(
                    PieEntry(
                        it.transaction.amount,
                        it.category.name,
                    )
                )
                colors.add(it.category.color)
            }

            val dataSet = PieDataSet(entries, "")

            dataSet.colors = colors

            withContext(Dispatchers.Main) {
                _pieData.value = PieData(dataSet)
            }
        }

    }
}