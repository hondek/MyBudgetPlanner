package com.quadrapps.budgetplanner.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.quadrapps.budgetplanner.domain.TransactionsListItem
import com.quadrapps.budgetplanner.databinding.ItemTransactionsListBinding
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class TransactionsListAdapter @Inject constructor(val clickListener: ClickListener) :
    ListAdapter<TransactionsListItem, TransactionsListAdapter.ViewHolder>(TransactionsListDiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, clickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(private val binding: ItemTransactionsListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: TransactionsListItem, clickListener: ClickListener) {
            binding.data = item
            binding.executePendingBindings()
            binding.clickListener = clickListener
            binding.circleView.circleColor = item.category_color
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemTransactionsListBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class TransactionsListDiffCallback : DiffUtil.ItemCallback<TransactionsListItem>() {

    override fun areItemsTheSame(oldItem: TransactionsListItem, newItem: TransactionsListItem): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: TransactionsListItem, newItem: TransactionsListItem): Boolean {
        return oldItem == newItem
    }

}

class ClickListener @Inject constructor() {

    var onItemClick: ((TransactionsListItem) -> Unit)? = null

    fun onClick(data: TransactionsListItem) {
        onItemClick?.invoke(data)
    }
}