package com.quadrapps.budgetplanner.ui.home

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.*
import com.quadrapps.budgetplanner.database.asDomainModel
import com.quadrapps.budgetplanner.domain.TransactionDetails
import com.quadrapps.budgetplanner.domain.TransactionsListItem
import com.quadrapps.budgetplanner.repository.CategoriesListRepository
import com.quadrapps.budgetplanner.repository.TransactionsListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class TransactionsListViewModel @Inject constructor(
    private val transactionsListRepository: TransactionsListRepository,
    private val categoriesListRepository: CategoriesListRepository,
) : ViewModel() {

    lateinit var data: LiveData<List<TransactionsListItem>> // = transactionsListRepository.transactions

    private val _currentTransaction = MutableLiveData<TransactionDetails>()
    val currentTransaction: LiveData<TransactionDetails> = _currentTransaction
    fun setCurrentTransaction(newCurrentTransaction: TransactionDetails) {
        _currentTransaction.value = newCurrentTransaction
    }

    fun setTransactionDate(newDate: Long)
    {
        var copy = _currentTransaction.value
        copy?.date = newDate/1000
        _currentTransaction.value = copy!!
    }

    fun initialize(type: Int) {
        data = transactionsListRepository.getTransactionsList(type)
    }

    private val _optionSelected = MutableLiveData<Int>()
    val optionSelected: LiveData<Int> = _optionSelected
    fun onOptionSelected(option: Int) {
        _optionSelected.value = option
    }

    fun putInitialData() {
        viewModelScope.launch(Dispatchers.IO) {
            transactionsListRepository.refreshTransactionsList()
            categoriesListRepository.putInitialData()
        }
    }

    fun getTransactionById(id: Long) {
        if (id == 0L) {
            _currentTransaction.value = TransactionDetails(0, "New transaction", 0, 0f, 0L)
            return
        }
        viewModelScope.launch(Dispatchers.IO) {
            val transaction = transactionsListRepository.getTransactionById(id)
            withContext(Dispatchers.Main) {
                _currentTransaction.value = transaction
            }
        }
    }

    fun saveTransaction(title: String, amount: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val updateTransaction = TransactionDetails(
                id = currentTransaction.value?.id,
                title = title,
                amount = amount.toFloat(),
                date = currentTransaction.value?.date,
                category_id = currentTransaction.value?.category_id
            )
            transactionsListRepository.updateTransaction(updateTransaction)
        }
    }
}