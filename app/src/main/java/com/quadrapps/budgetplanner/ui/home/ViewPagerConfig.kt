package com.quadrapps.budgetplanner.ui.home

import com.quadrapps.budgetplanner.R
import com.quadrapps.budgetplanner.ui.home.ExpensesFragment
import com.quadrapps.budgetplanner.ui.home.IncomeFragment

interface ViewPagerConfig {
    companion object {
        val tabList = listOf(
            R.string.expenses_tab, R.string.income_tab
        )
        val pagerFragments = listOf(
            ExpensesFragment(), IncomeFragment()
        )
    }
}