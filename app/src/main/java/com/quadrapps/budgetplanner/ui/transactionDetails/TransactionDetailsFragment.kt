package com.quadrapps.budgetplanner.ui.transactionDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.quadrapps.budgetplanner.R
import com.quadrapps.budgetplanner.databinding.FragmentTransactionDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TransactionDetailsFragment : Fragment() {
    private val viewModel: TransactionDetailsViewModel by viewModels()
    private val args: TransactionDetailsFragmentArgs by navArgs()

    private var _binding: FragmentTransactionDetailsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        saveInstanceState: Bundle?
    ): View {
        _binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_transaction_details, container, false
        )
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        binding.editTransactionButton.setOnClickListener {
            val bundle = Bundle()
            var transaction = viewModel.transactionDetails.value
            bundle.putLong("transaction", transaction?.id!!)
            findNavController().navigate(R.id.transaction_add_edit, bundle)
        }

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.refreshTransactionDetails(args.transaction)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getTransactionDetails(args.transaction)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}