package com.quadrapps.budgetplanner.ui.transactionDetails

import androidx.databinding.ObservableParcelable
import androidx.lifecycle.*
import com.quadrapps.budgetplanner.domain.CategoryDetails
import com.quadrapps.budgetplanner.domain.TransactionDetails
import com.quadrapps.budgetplanner.repository.CategoryDetailsRepository
import com.quadrapps.budgetplanner.repository.TransactionDetailsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class TransactionDetailsViewModel @Inject constructor(
    private val transactionDetailsRepository: TransactionDetailsRepository
) : ViewModel() {

    private val _transactionDetails = MutableLiveData<TransactionDetails>()
    val transactionDetails: LiveData<TransactionDetails> = _transactionDetails

    fun getTransactionDetails(transactionId: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            val transaction = transactionDetailsRepository.getTransactionDetails(transactionId)
            withContext(Dispatchers.Main) {
                _transactionDetails.value = transaction
            }
        }
    }

    fun refreshTransactionDetails(transactionId: Long) = viewModelScope.launch(Dispatchers.IO) {
        // transactionDetailsRepository.refreshTransactionDetails(transactionId)
    }
}